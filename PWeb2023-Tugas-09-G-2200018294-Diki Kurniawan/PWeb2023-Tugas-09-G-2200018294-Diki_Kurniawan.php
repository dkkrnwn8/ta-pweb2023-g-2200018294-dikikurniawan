<?php 
// Listing Program 9.1
echo "Listing 9.1<br>";
$gaji =1000000;
$pajak =0.1;
$thp = $gaji-($gaji*$pajak);
echo "Gaji sebelum pajak = Rp. $gaji <br>";
echo "Gaji yang dibawa pulang = Rp. $thp ";
echo "<br><br>";

// Listing Program 9.2
echo "Listing 9.2<br>";
$a = 5;
$b = 4;
echo "$a == $b : ".($a == $b);
echo "<br> $a != $b : ".($a != $b);
echo "<br> $a > $b : ".($a > $b);
echo "<br> $a < $b : ".($a < $b);
echo "<br> ($a == $b) && ($a > $b) : ".(($a != $b) && ($a > $b));
echo "<br> ($a == $b) || ($a > $b) : ".(($a != $b) || ($a > $b));
echo "<br><br>";

// Listing Modifikasi
echo "Listing Modifikasi<br>";
$X = 10;
$Y = 5;
echo "Diketahui X=10 dan Y=5";
echo "<br>";
echo "Hasil dari berbagai Operasi Aritmatika";
echo "<br>";
$jumlah = $X+$Y;
echo "X + Y = $jumlah ";
echo "<br>";
$kurang = $X-$Y;
echo "X - Y = $kurang ";
echo "<br>";
$kali = $X*$Y;
echo "X * Y = $kali ";
echo "<br>";
$bagi = $X/$Y;
echo "X / Y = $bagi ";
?>