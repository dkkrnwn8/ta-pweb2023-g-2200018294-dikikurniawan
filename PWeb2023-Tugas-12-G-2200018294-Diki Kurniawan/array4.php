<?php
    $arrNilai=array("Wawan"=>80, "Diki"=>90, "Jiro"=>75, "Agil"=>85);
    echo "Menampilkan isi array asosiatif dengan foreach : <br>";
    foreach($arrNilai as $nama => $nilai){
        echo "Nilai $nama=$nilai<br>";
    }
    
    reset($arrNilai);
    echo "<br>Menampilkan isi array asosiatif dengan while dan list : <br>";
    while(($nama = key($arrNilai)) !== null) {
        $nilai = current($arrNilai);
        echo "Nilai $nama=$nilai<br>";
        next($arrNilai);
    }
?>