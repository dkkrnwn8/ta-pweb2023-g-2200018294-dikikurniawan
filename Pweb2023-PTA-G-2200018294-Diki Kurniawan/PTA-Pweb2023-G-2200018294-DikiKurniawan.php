<!DOCTYPE html>
  <head>
    <title> Projek Tugas Akhir Diki Kurniawan </title>
    <link rel="stylesheet" href="style1.css">
   </head>
<body>
  <?php 
  //Fungsi Hit Counter
	function hit_count(){
    $filecounter="counter.txt"; 
    $fl=fopen($filecounter,"r+"); 
    $hit=fread($fl,filesize($filecounter)); 
  
    echo("<table width=100 align=center border=0 cellspacing=0 cellpadding=0 bordercolor=#0000FF><tr>");
    //echo("<tr bgcolor=\"#99FF66\">\n");
    echo("<td width=500 valign=middle align=center>"); 
    echo("<font face=verdana size=3 color=black><b>"); 
    echo("<img src=images/viewer.png align=center width=30 height=30 >");
    echo " : ";
    echo($hit); 
    echo("</b></font>"); 
    echo("</td>"); 
    echo("</tr></table>"); 
    fclose($fl); 
    $fl=fopen($filecounter,"w+"); 
    $hit=$hit+1; 
    fwrite($fl,$hit,strlen($hit)); 
    fclose($fl); 
    }
?>
  <header>

     <!--Menu Navigasi -->
  <nav>
    <div class="navbar">
      <div class="logo"><a href="#">Diki Kurniawan</a></div>
      <ul class="menu">
          <li><a href="#home">Beranda</a></li>
          <li><a href="#about">Tentang</a></li>
          <li><a href="#services">Pelayanan</a></li>
          <li><a href="#contact">Kontak</a></li>
          <li><a href="bukutamu.html">Buku Tamu</a><li>
          <li><a href="login.html">Daftar Tamu</a><li>
          <div class="cancel-btn">
            <i></i>
          </div>
      </ul>
      <div>
        <i></i>
        <i></i>
        <i></i>
      </div>
    </div>
    <div class="menu-btn">
      <i></i>
    </div>
  </nav>
  </header>

  <!-- Button scroll ke home -->
  <div class="scroll-button">
    <a href="#home"></i></a>
  </div>

<!-- Beranda  -->
 <section class="home" id="home">
   <div class="home-content">
     <div class="teks">
       <div class="teks-satu"><img src="images/animasi1.gif"></div>
       <div class="teks-dua">Saya Diki Kurniawan</div>
       <div class="teks-tiga">Mahasiswa Informatika UAD</div>
       <div class="teks-empat">Yogyakarta</div>
     </div>
     <div class="button">
       <button>
         <div class="teks-lima">
         <a href="#contact" style="text-decoration: none; color: whitesmoke;">Hubungi saya</a>
        </div>
      </button>
      <div class="logoicon">
      </br>
     <a href="https://www.instagram.com/dk.krnwn8/"><img src="images/instagram.png"> </a>
        &nbsp; <a href=" https://www.youtube.com/channel/UCoAlL5WBNJGKFVBRFt6mbTA"><img src="images/youtube.png"></a>
        &nbsp; <a href=" https://gitlab.com/dkkrnwn8/ta-pweb2023-g-2200018294-dikikurniawan"><img src="images/gitlab.png"></a>
      </div>
     </div>
   </div>
 </section>
 
<!-- Tentang -->
<section class="about" id="about">
  <div class="content">
    <div class="title"><span>Tentang Saya</span></div>
  <div class="about-details">
    <div class="left">
      <img src="images/foto.jpg" alt="">
    </div>
    <div class="right">
      <div class="topic"><b>Seni adalah passion saya</b></div>
      <p style="text-indent: 25px;">Hello Guys! Perkenalkan nama saya Diki Kurniawan, saya berasal dari Kepulauan Bangka Belitung, Indonesia. Saya adalah seorang mahasiswa di universitas ternama yaitu Universitas Ahmad Dahlan. Saya adalah seorang yang memiliki semangat tinggi dalam menjelajahi tempat baru dan juga memiliki jiwa kreatif sebagai seorang konten kreator. Saya merasa bahwa travelling adalah kesempatan yang luar biasa untuk mengeksplorasi dunia dan mengumpulkan pengalaman yang berharga.</p><p style="text-indent: 25px;">Ketika saya melakukan perjalanan, saya tidak hanya menikmati keindahan alam dan warisan budaya yang ditawarkan suatu tempat, tetapi juga melihatnya sebagai peluang untuk menghasilkan konten yang menginspirasi dan memotivasi orang lain. Saya suka mengabadikan momen-momen indah melalui fotografi, membuat video perjalanan yang menggugah, dan menulis cerita perjalanan yang mendalam.</p>
      <div class="button">
        <button><a href="CV_DIKI KURNIAWAN.pdf"  style="text-decoration: none; color: whitesmoke;" >Unduh CV</a></button>
      </div>
    </div>
  </div>
  </div>
</section>

<!-- Service -->
 <section class="services" id="services">
   <div class="content">
     <div class="title"><span>Menerima Pelayanan</span></div>
     <div class="boxes">
       <div class="box">
         <div class="icon-dua">
           <img src="images/journey.png">
           <i class=""></i>
       </div>
       <div class="topic">Journey</div>
       <p align="justify">Kami dengan senang hati menyambut Anda untuk merasakan pelayanan ajakan perjalanan yang luar biasa. Tim kami terdiri dari para profesional yang berdedikasi untuk memberikan pengalaman perjalanan yang tak terlupakan.</p>
     </div>
       <div class="box">
         <div class="icon-dua">
           <img src="images/travcont.png">
           <i class=""></i>
       </div>
       <div class="topic">Travcont</div>
       <p align="justify">Kami dengan antusias menyambut Anda untuk merasakan pelayanan ajakan perjalanan sekaligus konten yang serba bisa. Tim kami terdiri dari profesional yang berpengalaman dalam industri perjalanan dan kreativitas konten, yang akan memberikan pengalaman yang tak terlupakan dan konten yang menarik untuk Anda.</p>
     </div>
       <div class="box">
         <div class="icon-dua">
           <img src="images/konten.png">
           <i></i>
       </div>
       <div class="topic">Content</div>
       <p align="justify">Kami dengan antusias menyambut Anda untuk merasakan pelayanan ajakan konten yang menginspirasi. Tim kami terdiri dari kreator konten yang berbakat dan berpengalaman yang siap membantu Anda dalam menciptakan konten yang menarik dan kreatif.</p>
     </div>
   </div>
   </div>
 </section>

<!-- Contact  -->
<section class="contact" id="contact">
  <div class="content">
    <div class="title"><span>Kontak Saya</span></div>
    <div class="teks">
      <div class="topic">Ingin Menanyakan Seputar Pelayanan? Hubungi Kontak Dibawah Ini!</div>
      <div class="logoicon">
      <p>Email: <a href="mailto:2200018294@webmail.uad.ac.id" style="text-decoration: none; color: blue;">2200018294@webmail.uad.ac.id</a> No.Handphone: <a href="https://wa.link/020et7" style="text-decoration: none; color: blue;">+6288706515443</a>
      </br>Alamat: Jl. Ki Ageng Pemanahan, Tamanan, Banguntapan, Bantul, DIY, 55191 <p>
        </div>
      <div class="button-2">
        <button><a href="https://wa.link/020et7" style="text-decoration: none; color: whitesmoke;">Let's WhatsApp Chat</a></button>
      </div>

      <!-- Contact Us -->
    <section class="contact" id="contact">
      <div class="content">
        <div class="row">
          <div class="title"><span>Hubungi Kami</span>
          </div>
        </div>

        <div class="row">
          <div>
            <form name="data" method="post" action="">
              <div>
                <label for="nama">Nama</label>
                <input type="text" name="nama" id="nama"  placeholder="masukkan nama">
              </div>

              <div >
                <label for="email">Email</label>
                <input type="email" name="email" id="email"  placeholder="masukkan email">
              </div>

              <div >
                <label for="handphone">Nomor Handphone</label>
                <input type="text" name="handphone" id="handphone"  placeholder="masukkan nomor handphone">
              </div>

              <div >
                <label for="sosmed">Sosial Media URL</label>
                <input type="text" name="sosmed" id="sosmed"  placeholder="masukkan url">
              </div>

              <div>
                <label for="pesan">Pesan</label>
                <textarea  id="pesan" name="pesan" rows="10" placeholder="ketikkan pesan anda"></textarea>
              </div>
              <div class="button-2">
              <button Onclick="alertx(this.data)">Kirim Pesan </button>
            </div>
            </form>
            <p>Catatan: Mohon maaf jika ada keterlambatan, kami akan secepatnya menghubungi Anda melalui email.</p>
          <center>
            <br>
            <?php
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
              $nama = $_POST['nama'];
              $email = $_POST['email'];
              $handphone = $_POST['handphone'];
              $sosmed = $_POST['sosmed'];
              $pesan = $_POST['pesan'];
              $file = fopen("pesan.txt", "a");
              fwrite($file, "Nama: $nama" . PHP_EOL);
              fwrite($file, "Email: $email" . PHP_EOL);
              fwrite($file, "No. Handphone: $handphone" . PHP_EOL);
              fwrite($file, "Sosmed: $sosmed" . PHP_EOL);
              fwrite($file, "Pesan: $pesan" . PHP_EOL);
              fwrite($file, "---------------------------------\n");
              fclose($file);
            }
	          hit_count();
	          ?>
            </center>
          </div>
        </div>
      </div>
    </section>
<!-- akhir contact -->
    </div>
  </div>
</section>

<!-- Footer  -->
<footer>
  <div class="teks">
    <span>Created By <a href="#">Diki Kurniawan</a> | &#169; 2023</span>
  </div>
</footer>
  <script src="script.js"></script>
</body>
</html>

